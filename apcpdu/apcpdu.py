'''
APC Network PDU Controller

Payton Quackenbush
Modified by Sebastien Celles
Further modified by Stamatios Gkaitatzis

Tested with AP7900, but should work with similar models.
'''

import os
import sys
import re
import pexpect
import logging
from enum import Enum
import time
import threading

from apcpdu.outlets import Outlets
from apcpdu.load import Load

APCPDU_DEFAULT_HOST = os.environ.get('APCPDU_HOST', '192.168.1.2')
APCPDU_DEFAULT_USER = os.environ.get('APCPDU_USER', 'apc')
APCPDU_DEFAULT_PASSWORD = os.environ.get('APCPDU_PASSWORD', 'apc')

APCPDU_VERSION_PATTERN = re.compile(r' v(\d+\.\d+\.\d+)')

logging.basicConfig()

class ConnectionFailure(Exception):
    pass

class ApcPdu3:
    class Ctrl(Enum):
        Immediate_On = 1
        Immediate_Off = 2
        Immediate_Reboot = 3
        Delayed_On = 4
        Delayed_Off = 5
        Delayed_Reboot = 6

    class Conf(Enum):
        Name = 1
        Power_On_Delay = 2
        Power_Off_Delay = 3
        Reboot_Duration = 4

    APCPDU_ESCAPE = '\033'
    APCPDU_PROMPT = '> '

    def __init__(self, host, username=APCPDU_DEFAULT_USER,
                 password=APCPDU_DEFAULT_PASSWORD, loglevel='WARNING'):
        self.__logger = logging.getLogger('ApcPdu')
        self.__logger.setLevel(level=loglevel)
        self.__host = host
        self.__username = username
        self.__password = password
        self.__child = pexpect.spawn(None)
        self.__outlets = None
        self.__load = None
        self.__lock = threading.Lock()
    @property
    def host(self):
        return self.__host

    @property
    def outlets(self):
        return self.__outlets

    @property
    def load(self):
        return self.__load

    def connect(self):
        self.__lock.acquire(timeout=10)
        try:
            self.__child = pexpect.spawn(f'telnet {self.__host}')
            self.__child.timeout = 5
            self.__child.setecho(True)
            self.__child.expect('User Name : ')
            self.__sendln(self.__username)
            self.__child.expect('Password  : ')
            self.__sendln(self.__password)
            self.__child.expect(['-+ Control Console -+', pexpect.TIMEOUT])
            self.__child.expect(self.APCPDU_PROMPT)
        except pexpect.exceptions.EOF as e:
            self.__lock.release()
            self.__logger.warning(f'{self.__host}: Connection closed by remote host.')
            raise e
        except pexpect.exceptions.TIMEOUT as e:
            self.__lock.release()
            self.__logger.warning(f'{self.__host}: Connection timed out.')
            raise e
        self.__logger.debug(self.__child.before.decode('utf-8'))
        self.__logger.info(f'Connected to {self.__host}')

    def disconnect(self):
        try:
            self.__logger.debug(f'{self.__host}: Terminating.')
            self.__child.close(force=True)
            # Sleep for 0.2 to be certain that the connection is closed.
            # Else, spamming an action might throw an EOF during connection.
            time.sleep(0.2)
            self.__lock.release()
        except RuntimeError:
            pass


    def __sendln(self, s):
        self.__child.send(s + '\r\n')
        self.__logger.debug('Rx:' + s)

    def __escape_to_main(self, depth=6):
        for i in range(depth):
            self.__logger.debug("Tx: ESC")
            self.__child.send(self.APCPDU_ESCAPE)
        self.__child.expect('-+ Control Console -+')
        self.__child.expect(self.APCPDU_PROMPT)

    def update_outlets(self):
        self.__sendln('1')
        self.__sendln('2')
        self.__sendln('1')
        self.__child.expect("-+ Outlet Control/Configuration -+")
        self.__child.expect("<ESC>")
        self.__logger.debug(self.__child.before.decode('utf-8'))
        self.__outlets = Outlets.deserialize(self.__child.before)
        self.__escape_to_main(depth=3)
        return self.__outlets

    def update_load(self):
        self.__sendln('1')
        self.__sendln('1')
        self.__child.expect("-+ Phase Management -+")
        self.__child.expect(self.APCPDU_PROMPT)
        self.__logger.debug(self.__child.before.decode('utf-8'))
        s = self.__child.before
        self.__load = Load.parse(s)
        self.__escape_to_main(depth=2)
        return self.__load

    def control_outlet(self, outlet):
        self.__logger.debug(f'Control Outlet: {outlet}')
        self.__sendln('1')
        self.__sendln('2')
        self.__sendln('1')
        self.__sendln(str(outlet))
        self.__sendln('1')
        self.__child.expect("-+ Control Outlet -+")
        self.__child.expect(self.APCPDU_PROMPT)
        self.__logger.debug(self.__child.before.decode('utf-8'))

    def perform_action(self, action):
        self.__logger.debug(f'Action: {str(action)}')
        self.__sendln(str(action.value))
        self.__sendln('YES')
        self.__sendln('')
        self.__child.expect('-+ Control Outlet -+')
        self.__child.expect(self.APCPDU_PROMPT)
        s = self.__child.before
        if self.__outlets:
            self.__outlets.update(s)
        self.__escape_to_main(depth=5)

    def configure_outlet(self, outlet):
        self.__logger.debug(f'Configure Outlet: {self.outlet}')
        self.__sendln('1')
        self.__sendln('2')
        self.__sendln('1')
        self.__sendln(str(outlet))
        self.__sendln('2')
        self.__child.expect("-+ Configure Outlet -+")
        self.__child.expect(self.APCPDU_PROMPT)
        self.__logger.debug(self.__child.before.decode('utf-8'))

    def configure_parameter(self, parameter, value):
        if isinstance(parameter, self.Conf):
            parameter = [parameter]
            value = [value]
        for p, v in zip(parameter, value):
            self.__logger.info(f'Parameter: {str(p)} -> {str(v)}')
            self.__sendln(str(p.value))
            self.__sendln(str(v))
            self.__child.expect("-+ Configure Outlet -+")
            self.__child.expect(self.APCPDU_PROMPT)
        self.__sendln('5')
        self.__child.expect("-+ Configure Outlet -+")
        self.__child.expect(self.APCPDU_PROMPT)
        s = self.__child.before.decode('utf-8')
        self.__logger.debug(s)
        accept_changes = re.compile('.*5- Accept Changes\s*: (.*)')
        status = re.search(accept_changes, s).group(1)
        if 'Bad' in status:
            raise SystemExit(status)
        self.__escape_to_main(depth=5)

    def set_outlet_state(self, outlet, state, delay=0):
        # if not self.__lock.locked(): return
        self.__logger.info(f'Setting outlet state: {outlet} -> {state}, delay={delay}')
        if not delay:
            action = self.Ctrl.Immediate_On if state else self.Ctrl.Immediate_Off
        elif delay:
            action = self.Ctrl.Delayed_On if state else self.Ctrl.Delayed_Off
            param = self.Conf.Power_On_Delay if state else self.Conf.Power_Off_Delay
            self.configure_outlet(outlet)
            self.configure_parameter(param, delay)
        self.control_outlet(outlet)
        self.perform_action(action)

    def reboot_outlet(self, outlet, delay_off=0, delay_reboot=5):
        self.__logger.info(f'Reboot Outlet: {outlet}, delay_off={delay_off},'+
                           f'delay_reboot={delay_reboot}')
        if not delay_off:
            action = self.Ctrl.Immediate_Reboot
            if (delay_reboot < 5 or delay_reboot > 60):
                self.__logger.error("Bad reboot duration (5 sec to 60 sec).")
            self.configure_outlet(outlet)
        elif delay_off:
            action = self.Ctrl.Delayed_Reboot
            self.configure_outlet(outlet)
        self.configure_parameter([self.Conf.Power_Off_Delay, self.Conf.Reboot_Duration],
                                     [delay_off, delay_reboot])
        self.control_outlet(outlet)
        self.perform_action(action)

    def rename_outlet(self, outlet, new_name):
        self.configure_outlet(outlet)
        self.configure_parameter(self.Conf.Name, new_name)
