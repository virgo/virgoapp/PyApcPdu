#!/usr/bin/env python
from collections import UserDict
import re

APC_OUTLET_STATE_PATTERN1 = re.compile(r'(\d)- (.*) (OFF|ON)(\*?)')
APC_OUTLET_STATE_PATTERN2 = re.compile(r'Outlet (\d) (.*) (OFF|ON)(\*?)')
APC_OUTLET_MASTER_PATTERN = re.compile(r'.*Master Control\/Configuration.*')
APC_OUTLET_CHANGE_STATE_PATTERN = re.compile(r'Name\s*: (.*)\r\n\s*Outlet\s*: (\d)\r\n\s*State\s*: (OFF|ON)(\*?)')


class OutletParseException(Exception):
    pass


class Outlets(UserDict):
    class Outlet(UserDict):
        def __getattr__(self, attr):
            return self[attr]

    @classmethod
    def deserialize(cls, s):
        try:
            s = s.decode("utf-8")  # b'' -> string
        except (UnicodeError, AttributeError):
            pass
        s = s.strip()
        rows = s.split("\n")
        # Two forms of rows can be parsed, either:
        # 1- OUTLETNAME    ON*
        # or
        # Outlet 1 OUTLETNAME    ON*
        _d = cls()
        for outlet in rows:
            if APC_OUTLET_MASTER_PATTERN.search(outlet):
                continue
            match = APC_OUTLET_STATE_PATTERN1.search(outlet)
            if not match:
                match = APC_OUTLET_STATE_PATTERN2.search(outlet[3:])
            if not match:
                raise OutletParseException('Could not parse APC outlet state')

            s_idx, s_name, s_on, s_pending = match.groups()
            s_name = s_name.strip()
            state = False
            if s_on == "ON":
                state = True
            pending = False
            if s_pending == "*":
                pending = True
            _d[int(s_idx)] = cls.Outlet(idx=int(s_idx), name=s_name,
                                        state=state, pending=pending)
        return _d

    def serialize(self):
        msg = ''
        for idx, outlet in self.data.items():
            state = 'OFF'
            if outlet.state:
                state = 'ON'
            pending = ''
            if outlet.pending:
                pending = '*'
            msg += f' {idx}- {outlet.name}  {state}{pending}\n'
        return msg

    def update(self, s):
        s = s.strip().decode('utf-8')
        match = APC_OUTLET_CHANGE_STATE_PATTERN.search(s)
        if not match:
            raise OutletParseException('Could not parse APC outlet state')
        s_name, s_idx, s_on, s_pending = match.groups()
        state = False
        if s_on == "ON":
            state = True
        pending = False
        if s_pending == "*":
            pending = True
        self.data[int(s_idx)] = self.Outlet(idx=int(s_idx), name=s_name,
                                            state=state, pending=pending)

    def __getattr__(self, attr):
        if attr in vars(self):
            return self.__getitem__(attr)
        try:
            return [outlet[attr] for outlet in self.data.values()]
        except KeyError:
            return None

    def __setattr__(self, attr, value):
        if attr == 'data':
            super(Outlets, self).__setattr__(attr, value)
        else:
            for outlet, v in zip(self.data.values(), value):
                outlet[attr] = v

    def __len__(self):
        return len(self.data)

    def __iter__(self):
        return iter(self.data.values())
