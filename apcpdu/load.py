#!/usr/bin/env python
from collections import UserDict
import re
PHASE_LOAD_PATTERN = re.compile("(?<=Phase Load : )\s*(\d.\d)(\s*)")
PHASE_STATE_PATTERN = re.compile("(?<=Phase State: )(\w*)(?= Load)")
OVERLOAD_ALARM_PATTERN = re.compile("(?<=1- Overload Alarm Threshold\(amps\)       : )(\w*)")
NEAR_OVERLOAD_WARNING_PATTERN = re.compile("(?<=2- Near Overload Warning Threshold\(amps\): )(\w*)")
LOW_LOAD_WARNING_PATTERN = re.compile("(?<=3- Low Load Warning Threshold\(amps\)     : )(\w*)")

class Load(UserDict):
    @classmethod
    def parse(cls, text):
        match = re.search(PHASE_LOAD_PATTERN, str(text))
        if match:
            phase_load = float(match.group())

        match = re.search(PHASE_STATE_PATTERN, str(text))
        if match:
            phase_state = match.group()

        match = re.search(OVERLOAD_ALARM_PATTERN, str(text))
        if match:
            overload_alarm = float(match.group())

        match = re.search(NEAR_OVERLOAD_WARNING_PATTERN, str(text))
        if match:
            near_overload_warning = float(match.group())

        match = re.search(LOW_LOAD_WARNING_PATTERN, str(text))
        if match:
            low_load_warning = float(match.group())

        return cls(phase_load=phase_load,
                   phase_state=phase_state,
                   overload_alarm=overload_alarm,
                   near_overload_warning=near_overload_warning,
                   low_load_warning=low_load_warning,
                   unit='A')

    def __getattr__(self, attr):
        return self[attr]
