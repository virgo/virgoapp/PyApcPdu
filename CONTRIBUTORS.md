# Python APC contributors

- [Payton Quackenbush](https://github.com/quackenbush/) - initial author of https://github.com/quackenbush/APC/
- [Sébastien Celles](https://github.com/scls19fr/) - maintainer of https://github.com/scls19fr/APC
- [Stamatios Gkaitatzis](https://git.ligo.org/stamatios.gkaitatzis) - maintainer of the Virgo APC PDU fork.
