APC Network Power Management Controller
=======================================

Controls APC network PDU switches through pexpect and telnet.
Tested with the AP7921, but likely works with other models.

This handles locking of the device so that parallel calls will block, since
APC has a single telnet session.

Requirements
------------

- Python 3.x
- Python Expect (pexpect) library.  To install: 'pip install pexpect'
- APC with telnet network interface (tested on AP7921)

Installation
------------

```bash
$ pip install git+https://git.ligo.org/virgo/virgoapp/APC
```

or

Download source and run

```bash
$ python setup.py install
```

The APC needs to be set up with telnet enabled, and using a fixed IP address.
If a DHCP address is used, it may change, and you will have trouble connecting.

Usage
-----

### Display help
```
$ apcpdu-cli --help
usage: apcpdu-cli.py [-h] [--host HOST] [--user USER] [--password PASSWORD]
                     [--loglevel {DEBUG,INFO,WARNING,ERROR,CRITICAL}]
                     [--off OUTLET] [--on OUTLET] [--reboot OUTLET]
                     [--delay DELAY] [--duration DURATION] [--status] [--load]

Apc Pdu cli

optional arguments:
  -h, --help            show this help message and exit
  --host HOST           Set hostname
  --user USER           Set username
  --password PASSWORD   Set password
  --loglevel {DEBUG,INFO,WARNING,ERROR,CRITICAL}
                        Define verbosity.
  --off OUTLET          Turn off an outlet
  --on OUTLET           Turn on an outlet
  --reboot OUTLET       Reboot an outlet
  --delay DELAY         delay before on/off (-1 to 7200 sec, where -1=Never)
  --duration DURATION   reboot duration (5 to 60 sec)
  --status              Status of outlets
  --load                Load of outlets


```

### Outlet status
```
$ apcpdu-cli --status
#          Name                 Status
1          Outlet 1             ON
2          Outlet 2             OFF
3          Outlet 3             ON
4          Outlet 4             OFF
5          Outlet 5             OFF
6          Outlet 6             OFF
7          Outlet 7             ON
8          Outlet 8             OFF

```


### Power off a single port
#### Immediate
Example: power off port 4

```$ apcpdu-cli --off 4```

#### Delayed power off
Example: power off port 4 30 seconds later

```$ apcpdu-cli --off 4 --delay 30```

Environment Variables
---------------------

The following environment variables will override the APC connection:
- `$APCPDU_HOST`
- `$APCPDU_USER`
- `$APCPDU_PASSWORD`
